#!/bin/sh

# hint : try running the script with bash -i

echo "$-"

case "$-" in
  *i*)  
      echo "Interactive mode"
      ;;
  *)
      echo "Non interactive mode"
      ;;
esac

exit 0
