#!/bin/sh

case $MY_COUNT in
  1) 
    echo "Counter is 1"
    ;;
  2)
    echo "Counter is 2"
    ;;
  *)
    echo "Counter is neither 1 nor 2"
    ;;
esac

exit 0
