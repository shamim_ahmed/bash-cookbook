#!/bin/sh

if [ "$PS1" ] 
then
   echo "Interactive mode"
else
   echo "Non interactive mode"
fi

exit 0
