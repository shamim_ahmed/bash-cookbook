#!/bin/sh

# this scripts expects an environment variable named MY_VAR

[ "$MY_VAR" ] && echo "Found a value in MY_VAR" \
              || echo "Did not find a value in MY_VAR"

exit 0
