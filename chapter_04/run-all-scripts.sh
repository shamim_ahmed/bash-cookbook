#!/bin/sh

TARGET_DIR="$1"

if [ "$TARGET_DIR" = "" ]; then
  echo "Please provide the target directory name"
  exit 1
fi

echo "Running all executable scripts in $TARGET_DIR"

for my_file in `find $TARGET_DIR -type f`
do
  if [ -f "$my_file" -a -x "$my_file" ]; then
    echo "Executing $my_file ..."
    $my_file
  fi
done

exit 0
